
# BACK END

## Un desarrollador Front-end:

Se encarga de la parte visual (todo lo que llega a observar el usuario) de una solución digital.
Generalmente se comunica con uno o varios servicios creados por el desarrollador Back-end.

![frontend-backend](https://res.cloudinary.com/practicaldev/image/fetch/s--QFWF9cBP--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://blog.back4app.com/wp-content/uploads/2019/07/make-app-backend-frontend.png)

### Tecnologías de desarrollo que un Front-End debe conocer

Las tecnologías principales que debe conocer un desarrollador Front-End son: **HTML**, **CSS** y **Javascript**
Estas tecnologías tienen diversas metodologías y herramientas que ayudan a que el desarrollo sea mucho más fácil e interesante.
Como tendencias hoy en dia existen frameworks como **Bootstrap**, **Vuejs**, **Reactjs**, etc.

![frontend](https://i.ibb.co/Npmysyn/frontend.png)

### Actividades de un Front-end

1. Interactuar con el visual designer (UX Designer) |
2. Convertir diseños web a HTML + CSS
3. Desarrollar interactividad y adaptabilidad en una web
4. Desarrollar conexiones con servicios para obtener contenido dinámico

![frontend](https://www.redvirtual.bid/wp-content/uploads/2019/02/M%C3%A1ster-en-Arquitectura-Front-End-con-Bootstrap4-WP-Angular-660x330.jpg)

### Empresas que aportan al desarrollo Front-end

**Facebook** es uno de los más grandes colaboradores en el mundo del desarrollo Front-end, ellos liberaron algunas herramientas para el desarrollo en javascript el más conocido es el framework React que tiene un paradigma de desarrollo reactivo y modular el cual cambió mucho de la forma de como programar en javascript.

<img src="https://elandroidelibre.elespanol.com/wp-content/uploads/2013/01/facebook.jpg" width="300" height="150">

**Google** es otro de las empresas que apuesta al desarrollo del lado del cliente ofreciendo herramientas en javascript como Angular que es un framework muy usado tanto para hacer paginas web como aplicaciones móviles híbridas, google está siempre innovando en herramientas para el desarrollador Front-end y les da un alto soporte es por eso que muchos eligen sus tecnologías.

<img src="https://miro.medium.com/max/5120/1*Ps66WL6fkiuyNIynuon0Ww.png" width="300" height="160">

### ¿Cuál es el rol de un Front-end en las empresas del Perú?
**LINIO: Interfaz de carrito de compras**
Al ser la tienda online más grande del Perú se necesita que la interfaz sea fluida y adaptable a cualquier tipo teléfono para facilitar la compra.

![frontend](/img/frontend/linio.png)

**BCP: Interfaz de transacciones**
La interfaz del bcp ha cambiado con el tiempo haciéndola cada vez más simple y sencilla de usar. 
Es el trabajo de un front-end hacer esta interfaz interactiva y que no requiera de hacer recargos de página cada vez que se navega en las opciones.
![frontend](/img/frontend/bcp.png)

### ¿Cuál es el rol de un Front-end en las empresas del mundo?

Se encarga de hacer funcional la galería de fotos, que los post puedan visualizarse al ‘scrollear´, entre otros. Todo esto sin recargar la página.

### Analicemos:

¿Qué es lo que más destaca en las siguientes páginas web?
![frontend](/img/frontend/youtube.png)  ![frontend](/img/frontend/interbank.png)

### Conclusiones

![frontend](/img/frontend/youtube.png)
Para ver múltiples opciones no hace falta que la página recargue; sin embargo, te muestra sutilmente cómo va cargando.

 ![frontend](/img/frontend/interbank.png)
La web es interactiva desde los botones desplegables del menú y productos, hasta el background del formulario.

### Tendencias visuales en el diseño web 2020


\# elementosgrandes
	\# primeromóvil
		\# unapágina
			\# asimétricas

Más info: https://www.maxcf.es/tendencias-diseno-web-2020/


### Roadmap de un desarrollador Front-end

 ![frontend](https://res.cloudinary.com/practicaldev/image/fetch/s--TSIRHt2_--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://thepracticaldev.s3.amazonaws.com/i/8s6me5ycui2fy5ynb3gt.png)

### Tendencias para Front-end 2020:

- **PWA (Progressive Webs Apps)**, consumen otras tecnologías hasta API’s del navegador para poder realizar determinadas funcionalidades, así como también puede seguir ejecutándose en segundo plano sin necesidad de estar dentro del Navegador y muchas otras más funcionalidades, es como hacerte ver que tu web es una Aplicación. Ejemplo: https://hightide.earth/ es una aplicación que te permite buscar olas.
- **Componentes en la nube**, frameworks como Vuejs, Reactjs y Angular, han impulsado el uso de componentes, gracias a esto existe herramientas que nos permite subir nuestros componentes dentro de una comunidad donde podríamos poder reutilizarlas luego. Ejemplo: https://bit.dev/ nos permite subir nuestro components a la nube, para poder compartirlos.
- **Typescript**, a todos nos gusta tener un código limpio y fácil de entender, esto es por obvias razones. Cuando se desarrolla un proyecto, es necesario aportar soluciones, correcciones o cambios rápidamente y sin complicaciones, TypeScript ofrece esto y muchas otras características más. Además, es compatible con los Frameworks más populares de la actualidad. https://www.typescriptlang.org/.
- **Módulos ECMA Script vía CDN**, es como tener lo mismo que con bit, pero esta vez por CDN https://jspm.io/.
- **Web Assembly**, imagínate que aplicaciones Desktop como MySQL Workbench, Illustrator CC, etc. se puedan ejecutar desde el mismo navegador. Esta tecnología permite ejecutar el código de un Proyecto en código binario, y por ende, de manera muy rápida https://webassembly.org/.

*CDN : Es básicamente un conjunto de servidores ubicados en diferentes puntos de una red que contienen copias locales de ciertos contenidos (vídeos, imágenes, música, documentos, webs, etc.) que están almacenados en otros servidores, generalmente alejados geográficamente, de forma que sea posible servir dichos contenidos de manera más eficiente

## Un desarrollador Back-end:

Se encarga de la parte estructural y lógica de una solución digital y sirve para proveer de contenido solicitado por la parte de Front-end de una aplicación.

![frontend-backend](https://res.cloudinary.com/practicaldev/image/fetch/s--QFWF9cBP--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://blog.back4app.com/wp-content/uploads/2019/07/make-app-backend-frontend.png)



### Tecnologías de desarrollo que un Back-End debe conocer

Entra las tecnologías que debe conocer un desarrollador Back-end están:
- Lenguajes de programación y frameworks
- Base de datos
- Servidores web
- Protocolos SSL, HTTP, HTTPS

![frontend-backend](https://miro.medium.com/max/1200/1*0FAbAXug4XMepL7fGIvXFg.png)

### Lenguajes de programación y Frameworks
**Python:** fácil de aprender, veloz, semi-compilado y multipropósito. El framework para web más usado es Django. 
**PHP:** por ejemplo, el famoso gestor de contenidos WordPress usa por detrás PHP. Laravel es uno de los frameworks usados con este lenguaje. 
**Ruby:** es un lenguaje parecido al de Python, su framework más popular es Ruby on rails. 
**Node.js:** se está haciendo cada vez más popular debido a que usa el mismo lenguaje que en el lado cliente: JavaScript, su framework para web más usado es Express. 
**Java:** el lenguaje clásico y uno de los más demandados, su framework Spring. 
**ASP.NET:** es la plataforma de desarrollo web de Microsoft.
Más https://www.ecured.cu/Lenguaje_de_programaci%C3%B3n_(inform%C3%A1tica)

### Bases de datos

**MySQL:** base de datos relacional muy usada por millones de plataformas web actualmente es soportada por Oracle.
**Postgres:** base de datos relacional muy usada y de alto rendimiento y soporta base de datos geo espacial. 
**MongoDB:** base de datos no relacional muy usada y conocida para registrar grandes cantidades de datos a un costo de tiempo y rendimiento menor

### Servidores web y protocolos

**Nginx:** es uno de los servidor web / proxy inverso ligero y de alto rendimiento y proxy para protocolos de correo como IMAP y POP3. 
**Gunicorn:** es un servidor http que puede soportar desarrollos web en python con frameworks como django, flask entre otros. 
**HTTPS** es un protocolo de aplicación basado en el protocolo HTTP, destinado a la transferencia segura de datos de Hipertexto, es decir, es la versión segura de HTTP.

Más https://curiosoando.com/que-es-un-servidor-de-red

### Actividades de un Back-end
1. Analizar y diseñar modelos de base datos
2. Coordinar con el desarrollador Frontend la comunicación de los servicios
3. Desarrollar código que cumpla las condiciones del negocio
4. Seguridad y despliegue de código fuente


### Conceptos que debe conocer un desarrollador Back-end

**Performance:** un desarrollador Back-end se enfrenta a diferentes retos, como por ejemplo, tener una respuesta rápida a sus servicios y que no se caiga por múltiples peticiones de los usuarios, como soportar la alta concurrencia de personas, por ejemplo, en el proceso de registro de un evento masivo.

El desarrollador Back-end puede abordar estos problemas con diferentes soluciones, como Memcache, desarrollo transaccional, desarrollar colas de mensajes, entre otras opciones.

**Infraestructura:** un desarrollador Back-end debe entender algunos conceptos de linux y poder usar un servidor en dicho sistema operativo, puesto que los grandes servidores del mundo usan Linux.

Tener clara la infraestructura que está montado el código del Back-end es una tarea también del desarrollador Back-end puesto que es importante conocer cuál es el rendimiento de la aplicación con el código hecho

**Automatización:** una de las más grandes tareas del Backend es desarrollar procesos automáticos para poder cargar información o migrar a otras bases de datos o hacer procesos en horarios nocturnos para centralizar y depurar información
![frontend-backend](https://ubiqube.com/wp-content/uploads/2020/01/solution-Multi-Cloud-header.png)


Ejemplo: Los bancos actualizan los morosos de sus tarjetas de crédito todos los días a las 4:05 am para luego ejecutar envíos masivos de correos o SMS a los celulares de los deudores.

Más https://www.panel.es/zahori-automatizacion-de-pruebas/

### Roadmap de un desarrollador Back-end
![frontend](https://i.pinimg.com/originals/93/de/0e/93de0e042c2b9832e57093114abaf581.png)
### Tendencias para Back-end 2020:
- Lenguajes populares :
- ![frontend](https://www.nextu.com/wp-content/uploads/sites/3/2015/05/Los-Lenguajes-de-Codificacion-m-s-populares-en-2015-01.png)

- Frameworks Populares :
![frontend](https://www.campusmvp.es/recursos/image.axd?picture=/2017/2T/Frameworks-JavaScript.png)
- Base de datos Populares :
![frontend](https://revistadigital.inesem.es/informatica-y-tics/files/2016/01/SGBD-inesem.jpg)


### Analicemos:
- ¿Dónde crees que pueda estar el backend en las siguientes paginas?



### Conclusiones
Al momento de poder observar los videos populares, mas vistos o también los que se acomodan a nuestra búsqueda diaria.

Al momento de poder visualizar las promociones o también dentro de su banca segura.

- Podemos deducir que las tecnologías se van actualizando cada año, creciendo así el impacto dentro de nuestro mercado laboral. 
- Tanto el back-end como el front-end son indispensable para proyectos de desarrollo web.

