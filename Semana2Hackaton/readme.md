# Hackatón Semana 2
## **LOGRO**: Programar en pseudocódigo
**“Empecemos a programar!!!”**
Llegó el momento de demostrar lo aprendido durante esta semana, para esto deberás cumplir un reto: 

![](programemos.jpg)

**Resuelve los problemas de pseudocódigo en PSeInt y súbelo a Gitlab**

### Insumos para resolver el Reto

 - Materiales de clase de la semana 2
 - PSeInt http://pseint.sourceforge.net/


### Pasos a seguir para resolver el Reto
1. Descargar la ultima versión del proyecto
2. Crear la carpeta personal en la carpeta Semana2Hackaton
3. Resolver los problemas propuestos y guardarlos en archivos diferentes en la carpeta personal 
4. Sube tus cambios a tu repositorio git. 
5. Envía el **merge request** para integrar los cambios
### Solución del Reto
 - El proyecto debe tener una carpeta personal donde vamos a colocar cada problema en un archivo diferente.
 - Los problemas a resolver son los siguientes:
	 - Escribir un Pseudocódigo de un programa que permita leer la edad y peso de una persona y posteriormente imprimirla
	 - Escribir un Pseudocódigo que calcule el área de un triángulo recibiendo como entrada el valor de base y altura
	 - Escribir Pseudocódigo que calcule el área de un círculo.
	 - Escribir Pseudocodigo que dados 2 valores de entrada imprima siempre la división del mayor entre el menor.
	 - Escribir Pseudocódigo que lea de entrada 3 números y que indique cual es el mayor de ellos.
	 - Escribir un Pseudocódigo que lea 3 números los cuales significan una fecha (día, mes, año). Comprobar que sea válida la fecha, si no es valido que imprima un mensaje de error, y si es válida imprimir el mes con su nombre.
	 - Escribir un Pseudocodigo que pida la edad y el sexo y dependiendo si es hombre o mujer y si puede votar o no.
	 - Realice un Pseudocódigo que calcule la nómina salarial neto, de unos obreros cuyo trabajo se paga en horas. El cálculo se realiza de la siguiente forma:

	> Condiciones

		 Las primeras 35 horas a una tarifa fija.
		 Las horas extras se pagan a 1.5 más de la tarifa fija.
		 Las horas extras se pagan a 1.5 más de la tarifa fija.
		 Los impuestos a deducir de los trabajadores varían, según el sueldo mensual si el sueldo es menos a S./ 20,000.00 el sueldo es libre de impuesto y si es al contrario se cobrará un 20% de impuesto.

	- Escribir un Pseudocódigo que encuentre y despliegue los números primos entre uno y cien. Un número primo es divisible entre el mismo y la unidad por lo tanto un numero primo no puede ser par excepto el dos (2).
	- Hacer un Pseudocódigo que calcule el factorial de un número.
	- Hacer un Pseudocódigo que despliegue las tablas de multiplicar.
	- Algoritmo que lea números enteros hasta teclear 0, y nos muestre el máximo, el mínimo y la media de todos ellos. Piensa como debemos inicializar las variables.
	- Dada una secuencia de números leídos por teclado, que acabe con un –1, por ejemplo: 5,3,0,2,4,4,0,0,2,3,6,0,……,-1; Realizar el algoritmo que calcule la media aritmética. Suponemos que el usuario no insertara numero negativos.
	- Una tienda ofrece un descuento del 15% sobre el total de la compra durante el mes de octubre. Dado un mes y un importe, calcular cuál es la cantidad que se debe cobrar al cliente.
	- Dados 10 números enteros que se ingresan por teclado, calcular cuántos de ellos son pares, cuánto suman ellos y el promedio de los impares


 - El repositorio deberá contar con commits y su push en la rama principal (master)

### Reto cumplido
Para que el reto esté cumplido al 100% deberás tener cubierto los requisitos básicos expuestos: 
- Que se tenga en el gitlab 1 carpeta con el nombre (inicial del nombre y el apellido del estudiante) Dentro de ella, se agregarán los archivos de PSeInt con la solución de los ejercicios
- Revisar que el proyecto (repositorio) tenga un README.md con el nombre del colaborador y del proyecto. 
- Revisar que contengan commits y el push en la rama principal.